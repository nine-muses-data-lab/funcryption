# # try block to handle exception
try:

	path = "wee.jpg"
	print(path)
	fin = open(path, 'rb')
	image = fin.read()
	fin.close()

	image = bytearray(image)

	for i in range(50):
		print(image[i])

	path = "bee.jpg"
	print(path)
	fin = open(path, 'rb')
	image = fin.read()
	fin.close()

	image = bytearray(image)

	for i in range(50):
		print(image[i])

	path = "cee.jpg"
	print(path)
	fin = open(path, 'rb')
	image = fin.read()
	fin.close()

	image = bytearray(image)

	for i in range(50):
		print(image[i])
	

except Exception:
	print('Error caught : ', Exception.__name__)

